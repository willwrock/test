My most ambitious project, a full-fledged RPG. 
I tried to pack as much into this one as I could; serialization, animation states, scriptable objects, inheritance, coroutines, particles, sound, etc. 
As the scale grew, so did the number of problems. 
Buttons and memory management proved to be a headache, so I learned when it is and isn’t good to destroy objects such as dead enemies. 
I ended up attaching too much game logic to too many buttons, so I cut a few buttons out and replaced them with simple player input. 
The sheer number of things that need to be kept track of means streamlining is more important here than anywhere else.
If you cut corners in one place, it’ll end up biting you later. 
Enums were very useful for keeping track of character states, and I learned a lot about UI management working on this. 
Learning the 2D skeletal animation system was also very valuable, as it meant I could make animations in engine. 
At first, I made the sprites and animated them in Clip Studio Paint. 
This worked but wasn’t as efficient as making a skeleton with bones and IK’s. It also meant importing all the individual frames of the animation. 
I plan on continuing my work with this as a personal project; I’ve been working on adding dialogue and an overworld for the player to explore.
